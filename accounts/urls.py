


from os import path
from accounts.views import create_user, user_login, user_logout
from django.urls import path

urlpatterns = [
path("login/", user_login, name="login" ),
path("logout/", user_logout, name='logout'),
path("signup/", create_user, name='signup'),

]
