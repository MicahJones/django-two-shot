


from django import forms

from receipts.models import Receipt


class ReceiptForm(forms.ModelForm):
    class Meta:
        model = Receipt 
        fields = ['vendor', 'total', 'tax', 'date', 'category', 'account']
        widgets = {
            'date':forms.DateTimeInput(attrs={'type': 'datetime-local'})
        }
