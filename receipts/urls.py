

from django.shortcuts import redirect
from receipts.views import create_receipt, receipt_list
from django.urls import path


urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    
]
